﻿/* LookAtTransform.cs
 * Copyright Eddie Cameron & Grasshopper 2013
 * ----------------------------
 *
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SimpleMovers
{
    [ExecuteInEditMode]
    public class LookAtTransform : BehaviourBase
    {
        public Transform target;
        public bool reverse;
        public Vector3 worldUp = Vector3.zero;

        public RotationAxes constrainToLocalAxis;

        protected override void Update()
        {
            base.Update();

            if ( target )
            {
                if ( constrainToLocalAxis == RotationAxes.None )
                {
                    Vector3 lookPos = target.position;
                    if ( reverse )
                        lookPos = 2 * transform.position - target.position;
                    if ( worldUp == Vector3.zero )
                        transform.LookAt( lookPos );
                    else
                        transform.LookAt( lookPos, worldUp );
                }
                else
                {
                    transform.localRotation = Quaternion.identity; // reset rotation to fix constraints in case they change

                    var lookDir = target.position - transform.position;
                    if ( reverse )
                        lookDir = -lookDir;
                    Vector3 toTargetLocal = transform.WorldToLocalDirection( lookDir );
                    toTargetLocal[(int)constrainToLocalAxis - 1] = 0;    // no movement on constrained axis

                    transform.localRotation = Quaternion.LookRotation( toTargetLocal );
                }
            }
        }

        public enum RotationAxes
        {
            None,
            XAxis,
            YAxis,
        }
    }
}