﻿/* Rotator.cs
 * Copyright Eddie Cameron 2014
 * ----------------------------
 *
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Rotator : BehaviourBase {
    public Axis axis;
    public float speed; // in deg per sec

    protected override void Awake() {
        base.Awake();
    }

    protected override void Start() {
        base.Start();
    }

    protected override void Update() {
        base.Update();

        transform.Rotate( axis == Axis.X ? speed * Time.deltaTime : 0f,
            axis == Axis.Y ? speed * Time.deltaTime : 0,
            axis == Axis.Z ? speed * Time.deltaTime : 0 );
    }

    public enum Axis {
        X, Y, Z
    }
}