﻿/* SmoothSwitcher.cs
 * Copyright Eddie Cameron 2015
 * ----------------------------
 * When you want some value to switch slowly between on and off without all that fuss about whether it's already changing or whatnot
 * (eg: light dimmer switch, audio fade in/out, zoom)
 * -----------------------------
 * Can be added to an object at runtime, with SmoothSwitcher.SetupSmoothSwitcher(...)
 * Otherwise, make sure to set the SmoothSwitcher's onAmountSet event in the inspector, which will be called whenever the 'on amount' changes 
 *  (when setting an event listener method, make sure to choose a 'dynamic' method in the selection menu, otherwise it'll just call whatever number is in the box)
 * -----------------------------
 * eg use, for a light dimmer that starts turned off:
 * 
 * public class LightDimmer : MonoBehaviour {
 *      public float dimTime = 2f;
 *      public float onLightIntensity = 2f;
 * 
 *      SmoothSwitcher mySwitch;     
 * 
 *      void Start() {
 *          mySwitch = SmoothSwitcher.SetupSmoothSwitcher( this, dimTime, false, OnDimSet );
 *      }
 *      
 *      public void SwitchOn() {
 *          mySwitch.SwitchOn();
 *      }
 *      
 *      //... likewise for SwitchOff and Toggle, or can access switch directly, of course
 *      
 *      void OnDimSet( float dimAmount ) {
 *          light.intensity = dimAmount * onLightIntensity;
 *      }
 * }
 */

using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using MathUtils;

public class SmoothSwitcher : BehaviourBase {
    public float switchTime;
    public MathUtils.Easer.Equations easeType;

    [System.Serializable]
    public class SwitchAmountSetEvent : UnityEvent<float> { }

    public SwitchAmountSetEvent onAmountSet = new SwitchAmountSetEvent();

    [SerializeField]
    bool _startOn;
    public bool isOn { get; private set; }

    float _onAmount;
    public float onAmount {
        get { return _onAmount; }
        private set {
            _onAmount = value;
            onAmountSet.Invoke( value );
        }
    }

    float switchAmount;
    bool isSwitching;

    protected override void Awake() {
        base.Awake();

        isOn = _startOn;
    }

    protected override void Start() {
        base.Start();

        switchAmount = onAmount = isOn ? 1 : 0;
    }

    void Init( float switchTime, bool startOn, UnityAction<float> onSetOnAmountAction, Easer.Equations easeType = Easer.Equations.Linear ) {
        this.switchTime = switchTime;
        onAmountSet.AddListener( onSetOnAmountAction );
        this.easeType = easeType;

        isOn = startOn;
    }

    protected override void Update() {
        base.Update();

        if ( isSwitching ) {
            if ( isOn ) {
                // switching off
                switchAmount -= Time.deltaTime / switchTime;
                if ( switchAmount > 0f )
                    onAmount = (float)Easer.DoEquation( switchAmount, easeType );
                else {
                    // done switch
                    switchAmount = 0f;
                    isSwitching = false;
                    isOn = false;
                    onAmount = 0f;
                }
            }
            else {
                // switching on
                switchAmount += Time.deltaTime / switchTime;
                if ( switchAmount < 1f )
                    onAmount = (float)Easer.DoEquation( switchAmount, easeType );
                else {
                    // done switch
                    switchAmount = 1f;
                    isSwitching = false;
                    isOn = true;
                    onAmount = 1f;
                }
            }
        }
    }

    public void SwitchOn() {
        if ( isSwitching ) {
            if ( isOn ) {
                // currently switching off. Reverse
                isOn = false;
            }
        }
        else if ( !isOn ) {
            // switch on
            isSwitching = true;
        }
    }

    public void SwitchOff() {
        if ( isSwitching ) {
            if ( !isOn ) {
                // currently switching on. Reverse
                isOn = true;
            }
        }
        else if ( isOn ) {
            // switch off
            isSwitching = true;
        }
    }

    public void Toggle() {
        if ( isSwitching )
            isOn = !isOn; // reverse current switching
        else
            isSwitching = true; // start new switch
    }

    #region Auto setup
    public static SmoothSwitcher SetupSmoothSwitcher( Component onComponent, float switchTime, bool startOn, UnityAction<float> onSetOnAmountAction, Easer.Equations easeType = Easer.Equations.Linear ) {
        return SetupSmoothSwitcher( onComponent.gameObject, switchTime, startOn, onSetOnAmountAction, easeType );
    }

    public static SmoothSwitcher SetupSmoothSwitcher( GameObject onObj, float switchTime, bool startOn, UnityAction<float> onSetOnAmountAction, Easer.Equations easeType = Easer.Equations.Linear ) {
        var newSwitcher = onObj.AddComponent<SmoothSwitcher>();
        newSwitcher.Init( switchTime, startOn, onSetOnAmountAction, easeType );
        return newSwitcher;
    }
    #endregion
}