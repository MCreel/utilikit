# Utilikit #
## Helper scripts for Unity development ##

Utilikit is a collection of scripts & bits to make Unity development that little bit easier.
All scripts are Copyright Eddie Cameron unless they say otherwise.

Includes:

## Replacement classes ##

### BehaviourBase ###
Replaces MonoBehaviour as the base class for script components. It replaces Unity's own (now obsolete) component getters (e.g. .transform and .rigidbody) with caching versions.

### StaticBehaviour ###
A Unity-friendly singleton for your own static classes. Ensures there's only one in each scene at a time, and auto instantiates itself when needed.

### DebugExtras ###
Replace Debug. calls with DebugExtras. calls to have them auto stripped when a DEBUG flag isn't present. Also makes console printouts a little easier to read.

### ActionTime ###
A separately pausable Time system that doesn't affect unwanted coroutines, with helper events for when timescale changes.

### SafeEvents ###
Standard events with multiple listeners will stop executing when one listener throws an exception. Raise events through this class to make sure all listeners get the event, as well as getting a handy auto-null check.

## Utility Components ##

### Tweener ###
Static class to tween any object's location, rotation or scale in a given time. Supports easing and callbacks.

### SingleLerper ###
Lerps a value to keep up with a target.

### Trigger/Trigger2D ###
Sends events when objects of a given layer enter/stay/exit a trigger. Can also keep track of all matching colliders within the trigger.
Probably the script I use the most!

### Flasher ###
Just switches a renderer's colour back and forth. Seemed like a good idea at the time.

### SmoothSwitcher ###
To smoothly transition a value between on/off states without worrying about all those edge cases. (e.g. a light that dims slowly when turned off)

### LookAtTransform ###
Just makes the object face the given target. Useful for billboards.

### Oscillator ###
Make an object oscillate over a given axis

### Rotator ###
Rotates an object around a given axis

## Property Drawers ##

### EnumMask ###
Make any enum with the [Flags] attribute into a dropdown list, like the Unity layer mask selector.

### MinMaxRange ###
Make an int/float range field with sliders.

## Extension Methods ##

### Transform ###
* Set one axis of location, rotation, or scale directly _transform.SetZPos( 200 )_
* Um...just rename the Unity TransformPosition/Direction functions, because I can never remember which way round they go.
* Return the ray from this object going along it's forward axis. _transform.GetRay()_

### Collections ###
* Only add a value to a collection if it doesn't already exist. _AddUnique_ for list types, and _TryAdd_ for dictionaries
* Use foreach on a collection, removing null values as it goes
* Mapping function to copy to a new collection
* RandomElement and Shuffle

### Gameobject/Component ###
* GetComponent(s)WithInterface

## Misc ##

### MathUtils ###
A bunch of helpful math functions. Normalise angles, intersect lines & planes, generate min hulls, number formatting, easing (thanks to Darren David), and combinatorics (thanks to Alex Reguiero)

### Pathfinder ###
Generic, basic A* pathfinding algorithm

### CreateNewLine ###
Use Alt-Enter to add a new line to inspector text-boxes in Windows