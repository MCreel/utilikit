﻿/* MinMaxRangeAttribute.cs
 * by Eddie Cameron - For the public domain
 * ----------------------------
 * 
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Apply a [MinMaxRange( minLimit, maxLimit )] attribute to a MinMaxRange instance to control the limits and to show a
/// slider in the inspector
/// </summary>
public class MinMaxRangeAttribute : PropertyAttribute
{
    public float minLimit, maxLimit;

    public MinMaxRangeAttribute( float minLimit, float maxLimit )
    {
        this.minLimit = minLimit;
        this.maxLimit = maxLimit;
    }
}

/// <summary>
/// Replace twin float range values (eg: <c>float minSpeed, maxSpeed;</c> becomes <c>MinMaxRangeFloat speed;</c>)
/// </summary>
[System.Serializable]
public class MinMaxRangeFloat
{
    public float rangeStart, rangeEnd;

    public float rangeSize { get{ return rangeEnd - rangeStart; } }

    public float Lerp( float t )
    {
        return Mathf.Lerp( rangeStart, rangeEnd, t );
    }

    /// <summary>
    /// Return random value between rangeStart and rangeEnd (INCLUSIVE)
    /// </summary>
    /// <returns></returns>
    public float GetRandomValue()
    {
        return Random.Range( rangeStart, rangeEnd );
    }

    public bool IsValueInRange( float f ) {
        return f >= rangeStart && f <= rangeEnd;
    }

    public static implicit operator MinMaxRangeFloat( float f )
    {
        MinMaxRangeFloat range = new MinMaxRangeFloat();
        range.rangeStart = range.rangeEnd = f;

        return range;
    }
}

/// <summary>
/// Replace twin int range values (eg: <c>int minNum, maxNum;</c> becomes <c>MinMaxRangeInt num;</c>
/// </summary>
[System.Serializable]
public class MinMaxRangeInt
{
    public int rangeStart, rangeEnd;

    public int rangeSize { get{ return rangeEnd - rangeStart; } }

    /// <summary>
    /// Random between rangeStart (INCLUSIVE) and rangeEnd (EXCLUSIVEW)
    /// </summary>
    /// <returns>The random value.</returns>
    public int GetRandomValue()
    {
        return Random.Range( rangeStart, rangeEnd );
    }

    public static implicit operator MinMaxRangeInt( int i )
    {
        MinMaxRangeInt range = new MinMaxRangeInt();
        range.rangeStart = range.rangeEnd = i;

        return range;
    }
}