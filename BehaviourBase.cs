/* BehaviourBase.cs
 * ----------------------------
 * Copyright Eddie Cameron & Grasshopper 2014
 * Open-sourced under the MIT licence
 * ----------------------------
 * 
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Replacement for stock MonoBehaviour. To be used for all scripts
/// - Make sure to override (rather than hide with 'new') any unity methods like Start() or Update(), and call <c>base.Method();</c> so 
///   that any future code here will be executed
/// </summary>
public class BehaviourBase : MonoBehaviour
{
    /// <summary>
    /// Cached access to Transform
    /// </summary>
    public new Transform transform
    {
        get
        {
            if ( !_transform )
                _transform = GetComponent<Transform>();

            return _transform;
        }
    }
    Transform _transform;

    /// <summary>
    /// Cached access to Rigidbody
    /// </summary>
    public Rigidbody rigidbody {
        get {
            if ( !_rigidbody )
                _rigidbody = GetComponent<Rigidbody>();

            return _rigidbody;
        }
    }
    Rigidbody _rigidbody;

    /// <summary>
    /// Cached access to Renderer
    /// </summary>
    public Renderer renderer
    {
        get
        {
            if ( !_renderer )
                _renderer = GetComponent<Renderer>();

            return _renderer;
        }
    }
    Renderer _renderer;

    /// <summary>
    /// Cached access to AudioSource
    /// </summary>
    public AudioSource audio
    {
        get
        {
            if ( !_audioSource )
                _audioSource = GetComponent<AudioSource>();

            return _audioSource;
        }
    }
    AudioSource _audioSource;

    /// <summary>
    /// Cached access to Camera
    /// </summary>
    public Camera camera
    {
        get
        {
            if ( !_camera )
                _camera = GetComponent<Camera>();

            return _camera;
        }
    }
    Camera _camera;

    /// <summary>
    /// Called when component is placed on GameObject, or reset in inspector
    /// </summary>
    protected virtual void Reset()
    {

    }

    /// <summary>
    /// Called when object is instantiated
    /// </summary>
    /// <remarks>
    /// Called before Instantiate() returns
    /// </remarks>
    protected virtual void Awake()
    {
		
    }

    /// <summary>
    /// Called after awake when obect is instantiated
    /// </summary>
    /// <remarks>
    /// Called before end of frame that object was Instantiate()d
    /// </remarks>
    protected virtual void Start()
    {
		
    }

    /// <summary>
    /// Called when object is instantiated, made active, or component enabled
    /// </summary>
    protected virtual void OnEnable()
    {
    }

    /// <summary>
    /// Called when object is destroyed, made inactive, or component disabled
    /// </summary>
    protected virtual void OnDisable()
    {
  
    }

    /// <summary>
    /// Called when object is destroyed
    /// </summary>
    protected virtual void OnDestroy() {

    }

    /// <summary>
    /// Called every frame if enabled
    /// </summary>
    protected virtual void Update()
    {
		
    }

    /// <summary>
    /// Called after all Updates have been called
    /// </summary>
    protected virtual void LateUpdate()
    {

    }

    /// <summary>
    /// Called each physics frame
    /// </summary>
    protected virtual void FixedUpdate()
    {

    }

    /// <summary>
    /// Use to do stuff with a gradually changing value
    /// </summary>
    /// <remarks>
    /// Call with StartCoroutine
    /// </remarks>
    /// <param name="from">Value to start from</param>
    /// <param name="to">Value to end at</param>
    /// <param name="time">Time for value to change</param>
    /// <param name="lerpedFunction">Function that does something with T. Called each frame and given the updated value</param>
    /// <param name="finishFunction">Function called when change is finished</param>
    /// <param name="useActionTime">Whether should use <see cref="ActionTime"/></param>
    protected IEnumerator LerpValue( float from, float to, float time, System.Action<float> lerpedFunction, System.Action finishFunction = null, bool useActionTime = false, MathUtils.Easer.Equations easeType = MathUtils.Easer.Equations.Linear )
    {
        if ( useActionTime )
            yield return StartCoroutine( LerpFunctionActionTime( time, f => lerpedFunction( Mathf.Lerp( from, to, f ) ), easeType ) );
        else
            yield return StartCoroutine( LerpFunctionRealTime( time, f => lerpedFunction( Mathf.Lerp( from, to, f ) ), easeType ) );

        if ( finishFunction != null )
            finishFunction();
    }
    
    /// <summary>
    /// Use to do stuff with a gradually changing vector
    /// </summary>
    /// <remarks>
    /// Call with StartCoroutine
    /// </remarks>
    /// <param name="from">Vector to start from</param>
    /// <param name="to">Vector to end at</param>
    /// <param name="time">Time for vector to change</param>
    /// <param name="lerpedFunction">Function that does something with vector. Called each frame and given the updated value of the vector</param>
    /// <param name="finishFunction">Function called when change is finished</param>
    /// <param name="useActionTime">Whether should use <see cref="ActionTime"/></param>
    protected IEnumerator LerpVector( Vector3 from, Vector3 to, float time, System.Action<Vector3> lerpedFunction, System.Action finishFunction = null, bool useActionTime = false, MathUtils.Easer.Equations easeType = MathUtils.Easer.Equations.Linear )
    {
        if ( useActionTime )
            yield return StartCoroutine( LerpFunctionActionTime( time, f => lerpedFunction( Vector3.Lerp( from, to, f ) ), easeType ) );
        else
            yield return StartCoroutine( LerpFunctionRealTime( time, f => lerpedFunction( Vector3.Lerp( from, to, f ) ), easeType ) );

        if ( finishFunction != null )
            finishFunction();
    }

    /// <summary>
    /// Use to do stuff with a gradually changing rotation
    /// </summary>
    /// <remarks>
    /// Call with StartCoroutine
    /// </remarks>
    /// <param name="from">Quaternion to start from</param>
    /// <param name="to">Quaternion to end at</param>
    /// <param name="time">Time for rotation to change</param>
    /// <param name="lerpedFunction">Function that does something with rotation. Called each frame and given the updated value of the rotation</param>
    /// <param name="finishFunction">Function called when change is finished</param>
    /// <param name="useActionTime">Whether should use <see cref="ActionTime"/></param>
    protected IEnumerator LerpQuaternion( Quaternion from, Quaternion to, float time, System.Action<Quaternion> lerpedFunction, System.Action finishFunction = null, bool useActionTime = false, MathUtils.Easer.Equations easeType = MathUtils.Easer.Equations.Linear )
    {
        if ( useActionTime )
            yield return StartCoroutine( LerpFunctionActionTime( time, f => lerpedFunction( Quaternion.Lerp( from, to, f ) ), easeType ) );
        else
            yield return StartCoroutine( LerpFunctionRealTime( time, f => lerpedFunction( Quaternion.Lerp( from, to, f ) ), easeType ) );

        if ( finishFunction != null )
            finishFunction();
    }

    protected IEnumerator LerpFunctionRealTime( float time, System.Action<float> lerpResultFunction, MathUtils.Easer.Equations easeType = MathUtils.Easer.Equations.Linear )
    {
        float amount = 0;
        while ( amount < 1f )
        {
            lerpResultFunction( (float)MathUtils.Easer.DoEquation( amount, easeType ) );
            yield return 0;
            amount += Time.deltaTime / time;
        }

        lerpResultFunction( 1f );
    }

    protected IEnumerator LerpFunctionActionTime( float time, System.Action<float> lerpResultFunction, MathUtils.Easer.Equations easeType = MathUtils.Easer.Equations.Linear )
    {
        float amount = 0;
        while ( amount < 1f )
        {
            lerpResultFunction( (float)MathUtils.Easer.DoEquation( amount, easeType ) );
            yield return ActionTime.WaitOneActionFrame();
            amount += ActionTime.deltaTime / time;
        }

        lerpResultFunction( 1f );
    }
    
    public Coroutine<T> StartCoroutine<T>( IEnumerator coroutine ) {
        Coroutine<T> coroutineObject = new Coroutine<T>();
        coroutineObject.coroutine = StartCoroutine( coroutineObject.InternalRoutine( coroutine ) );
        return coroutineObject;
    }

}

/// <summary>
/// Base class for all Singletons, T is the actual type
/// </summary>
/// <typeparam name="T">Actual type of subclass</typeparam>
/// <example>
/// <code>public class MyClass : StaticBehaviour&lt;MyClass&gt; {...}
/// </code>
/// </example>
/// <remarks>
/// Use when there will only be one instance of a script in the scene. Makes access to non-static variables from static methods easy (with instance.fieldName)
/// </remarks>
public class StaticBehaviour<T> : BehaviourBase where T : BehaviourBase
{
    static T _instance;
    static bool noSpawn;

    protected static bool inScene { get { return _instance; } }
    protected static T instance
    {
        get
        {
            if ( !_instance )
                UpdateInstance();
            return _instance;
        }
    }

    protected override void Awake()
    {
        if ( _instance )
        {
            if ( _instance != this )
            {
                DebugExtras.LogWarning( "Duplicate instance of " + GetType() + " found. Removing " + name + " and gameobject" );
                Destroy( gameObject );
                return;
            }
        }
        else
            UpdateInstance();
		
        base.Awake();
    }

    protected static bool CheckInstance()
    {
        return instance;
    }

    static void UpdateInstance()
    {
        _instance = GameObject.FindObjectOfType( typeof( T ) ) as T;
        if ( !_instance && !noSpawn )
        {
            DebugExtras.LogWarning( "No object of type : " + typeof( T ) + " found in scene. Creating" );
            _instance = new GameObject( typeof( T ).ToString() ).AddComponent<T>();
        }
    }

    void OnApplicationQuit()
    {
        noSpawn = true;
    }
}

/// <summary>
/// Base class for a nullable type, so you can use <c>if ( myClass ) {...}</c> to check for null
/// </summary>
public class Nullable
{
    public static implicit operator bool( Nullable me )
    {
        return me != null;
    }
}