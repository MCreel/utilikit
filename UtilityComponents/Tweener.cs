﻿/* Tweener.cs
 * ----------------------------
 * Copyright Eddie Cameron & Grasshopper, 2014
 * Open-sourced under the MIT licence
 * ----------------------------
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MathUtils;

/// <summary>
/// Does simple movement/rotation tweening
/// </summary>
/// <remarks>
/// Can add to object and control directly, or just use the static methods
/// </remarks>
public class Tweener : BehaviourBase
{
    /// <summary>
    /// Holds info about each tween
    /// </summary>
    struct TweenInfo
    {
        /// <summary>
        /// How long will tween take
        /// </summary>
        public float tweenTime;
        /// <summary>
        /// Is tween in local or world space
        /// </summary>
        public Space space;

        public Easer.Equations easeType;
        /// <summary>
        /// If a rotation tween, what's the target
        /// </summary>
        public Quaternion targetRot;
        /// <summary>
        /// If a movement tween, what's the target
        /// </summary>
        public Vector3 targetPos;
        public Vector3 targetScale;

        public TweenInfo( float tweenTime, Space space, Easer.Equations easeType )
        {
            this.tweenTime = tweenTime;
            this.space = space;
            this.easeType = easeType;

            this.targetRot = Quaternion.identity;
            this.targetPos = Vector3.zero;
            this.targetScale = Vector3.one;
        }
    }

    /// <summary>
    /// Is this component destroyed when tween finishes?
    /// </summary>
    [HideInInspector]
    public bool destroyOnComplete;

    bool moving, rotating, scaling;

    /// <summary>
    /// Movement tween finished
    /// </summary>
    public event System.Action FinishedMove;
    /// <summary>
    /// Rotation tween finished
    /// </summary>
    public event System.Action FinishedRotation;
    public event System.Action FinishedScale;

    #region Manager

    static Dictionary<GameObject, Tweener> sceneTweeners = new Dictionary<GameObject, Tweener>(); // tweeners made at runtime

    /// <summary>
    /// Move any transform to <paramref name="location"/>
    /// </summary>
    /// <param name="objToMove"></param>
    /// <param name="location"></param>
    /// <param name="inTime"></param>
    /// <param name="space">Default: Local space</param>
    /// <returns></returns>
    public static Tweener MoveObjectTo( GameObject objToMove, Vector3 location, float inTime, Space space = Space.Self, Easer.Equations easeType = Easer.Equations.Linear )
    {
        Tweener tweener;
        if ( !sceneTweeners.TryGetValue( objToMove, out tweener ) )
        {
            tweener = objToMove.AddComponent<Tweener>();
            tweener.destroyOnComplete = true;
        }
        tweener.MoveTo( location, inTime, space, easeType );

        return tweener;
    }

    public static Tweener MoveObjectTo( Component objToMove, Vector3 location, float inTime, Space space = Space.Self, Easer.Equations easeType = Easer.Equations.Linear )
    {
        return MoveObjectTo( objToMove.gameObject, location, inTime, space, easeType );
    }
    
    /// <summary>
    /// Stop movement tween, if any, on <paramref name="onObj"/>
    /// </summary>
    /// <param name="onObj"></param>
    public static void StopMove( GameObject onObj )
    {
        Tweener tweener;
        if ( sceneTweeners.TryGetValue( onObj, out tweener ) )
            tweener.StopMove();
        else
            Debug.Log( "No tween on object: " + onObj.name ); 
    }

    /// <summary>
    /// Rotate any transform to <paramref name="rotation"/>
    /// </summary>
    /// <param name="objToRotate"></param>
    /// <param name="rotation"></param>
    /// <param name="inTime"></param>
    /// <param name="space">Default: Local space</param>
    /// <returns></returns>
    public static Tweener RotateObjectTo( GameObject objToRotate, Quaternion rotation, float inTime, Space space = Space.Self )
    {
        Tweener tweener;
        if ( !sceneTweeners.TryGetValue( objToRotate, out tweener ) )
        {
            tweener = objToRotate.AddComponent<Tweener>();
            tweener.destroyOnComplete = true;
        }
        tweener.RotateTo( rotation, inTime, space );

        return tweener;
    }

    public static Tweener RotateObjectTo( Component objToRotate, Quaternion rotation, float inTime, Space space = Space.Self )
    {
        return RotateObjectTo( objToRotate.gameObject, rotation, inTime, space );
    }

    /// <summary>
    /// Stop rotation tween, if any, on <paramref name="onObj"/>
    /// </summary>
    /// <param name="onObj"></param>
    public static void StopRotation( GameObject onObj )
    {
        Tweener tweener;
        if ( sceneTweeners.TryGetValue( onObj, out tweener ) )
            tweener.StopRotation();
        else
            Debug.Log( "No tween on object: " + onObj.name ); 
    }

    public static Tweener ScaleObjectTo( GameObject objToScale, Vector3 scale, float inTime )
    {
        Tweener tweener;
        if ( !sceneTweeners.TryGetValue( objToScale, out tweener ) )
            tweener = objToScale.AddComponent<Tweener>();
        tweener.ScaleTo( scale, inTime );

        return tweener;
    }

    public static Tweener ScaleObjectTo( Component objToScale, Vector3 scale, float inTime )
    {
        return ScaleObjectTo( objToScale.gameObject, scale, inTime );
    }

    public static void StopScale( GameObject onObj )
    {
        Tweener tweener;
        if ( sceneTweeners.TryGetValue( onObj, out tweener ) )
            tweener.StopScale();
        else
            Debug.Log( "No tween on object: " + onObj.name );
    }
    #endregion

    protected override void Awake()
    {
        base.Awake();

        if ( !sceneTweeners.TryAdd( gameObject, this ) )
        {
            DebugExtras.LogWarning( "Gameobject already has a tweener on it. Destroying. " + name );
            Destroy( this );
        }
    }

    protected override void OnDestroy() {
        base.OnDestroy();
        sceneTweeners.Remove( gameObject );
    }

    /// <summary>
    /// Move object to given location in given time
    /// Replaces any current movements
    /// </summary>
    /// <param name="location">Location.</param>
    /// <param name="inTime">In time.</param>
    /// <param name="space">World or local space (default = local)</param>
    /// <param name="easeType">Ease type (default = linear)</param>
    public Coroutine MoveTo( Vector3 location, float inTime, Space space = Space.Self, Easer.Equations easeType = Easer.Equations.Linear )
    {
        StopCoroutine( "Move" );
        var tweenInfo = new TweenInfo( inTime, space, easeType );
        tweenInfo.targetPos = location;
        return StartCoroutine( "Move", tweenInfo );
    }

    /// <summary>
    /// Stop any current movement. Finish event not raised
    /// </summary>
    public void StopMove()
    {
        StopCoroutine( "Move" );
        if ( moving )
        {
            moving = false;
            FinishedAction();
        }
    }

    IEnumerator Move( TweenInfo tween )
    {
        moving = true;
        var startPos = tween.space == Space.Self ? transform.localPosition : transform.position;
        float amount = 0;
        while ( amount < 1f )
        {
            amount += Time.deltaTime / tween.tweenTime;
            var moveTo = Vector3.Lerp( startPos, tween.targetPos, (float)Easer.DoEquation( amount, tween.easeType ) );
            if ( tween.space == Space.Self )
                transform.localPosition = moveTo;
            else
                transform.position = moveTo;
            if ( amount >= 1f )
                break;

            yield return 0;
        }

        moving = false;
        if ( FinishedMove != null )
            FinishedMove();

        FinishedAction();
    }
    
    /// <summary>
    /// Rotate object to given rotation in given time
    /// Replaces any current rotations
    /// </summary>
    /// <param name="rotation">Rotation.</param>
    /// <param name="inTime">In time.</param>
    /// <param name="space">World or local space (default = local)</param>
    public Coroutine RotateTo( Quaternion rotation, float inTime, Space space = Space.Self )
    {
        StopCoroutine( "Rotate" );
        var tweenInfo = new TweenInfo( inTime, space, Easer.Equations.Linear );
        tweenInfo.targetRot = rotation;
        return StartCoroutine( "Rotate", tweenInfo );
    }

    /// <summary>
    /// Stop any current rotation. Finish event not raised
    /// </summary>
    public void StopRotation()
    {
        StopCoroutine( "Rotate" );
        if ( rotating )
        {
            rotating = false;
            FinishedAction();
        }
    }

    IEnumerator Rotate( TweenInfo tween )
    {
        rotating = true;
        var startRot = tween.space == Space.Self ? transform.localRotation : transform.rotation;

        float amount = 0;
        while ( amount < 1f )
        {
            amount += Time.deltaTime / tween.tweenTime;
            var rotateTo = Quaternion.Lerp( startRot, tween.targetRot, amount );
            if ( tween.space == Space.Self )
                transform.localRotation = rotateTo;
            else
                transform.rotation = rotateTo;
            if ( amount >= 1f )
                break;

            yield return 0;
        }

        rotating = false;
        if ( FinishedRotation != null )
            FinishedRotation();

        FinishedAction();
    }


    /// <summary>
    /// Scale object to given scale in given time
    /// Replaces any current scalings
    /// </summary>
    /// <param name="scale">Scale.</param>
    /// <param name="inTime">In time.</param>
    public Coroutine ScaleTo( Vector3 scale, float inTime )
    {
        StopCoroutine( "Scale" );
        var tweenInfo = new TweenInfo( inTime, Space.Self, Easer.Equations.Linear );
        tweenInfo.targetScale = scale;
        return StartCoroutine( "Scale", tweenInfo );
    }

    /// <summary>
    /// Stop any current movement. Finish event not raised
    /// </summary>
    public void StopScale()
    {
        StopCoroutine( "Scale" );
        if ( scaling )
        {
            scaling = false;
            FinishedAction();
        }
    }

    IEnumerator Scale( TweenInfo tween )
    {
        scaling = true;
        var startScale = transform.localScale;

        float amount = 0;
        while ( amount < 1f )
        {
            amount += Time.deltaTime / tween.tweenTime;
            var scaleTo = Vector3.Lerp( startScale, tween.targetScale, amount );
            transform.localScale = scaleTo;

            if ( amount >= 1f )
                break;

            yield return 0;
        }

        scaling = false;
        if ( FinishedScale != null )
            FinishedScale();

        FinishedAction();
    }

    void FinishedAction()
    {
        if ( !rotating && !moving && !scaling && destroyOnComplete )
            Destroy( this );
    }
}
