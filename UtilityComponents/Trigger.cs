/* Trigger.cs
 * ----------------------------
 * Copyright Eddie Cameron & Grasshopper, 2014
 * Open-sourced under the MIT licence
 * ----------------------------
 * 
 */

using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

/// <summary>
/// Drop on a trigger collider to have it send events when specified layers enter/stay/exit.
/// </summary>
/// <remarks>
/// Useful for triggers on child objects and for making dealing with layer mask problems far easier
/// </remarks>
[RequireComponent( typeof( Collider ) )]
public class Trigger : BehaviourBase
{
    /// <summary>
    /// What layers are trigger activated by
    /// </summary>
    public LayerMask layers; // Much faster, overrides tag string if set
    /// <summary>
    /// Does the trigger keep track of colliders within itself?
    /// </summary>
    public bool saveCollidersWithin;
    /// <summary>
    /// Which tags activate this trigger
    /// </summary>
    /// <remarks>Layers is much faster</remarks>
    public string tagToCheck = "None";

    bool checkNone;
    bool checkString;
    HashSet<Collider> within = new HashSet<Collider>();
    HashSet<Collider> tempWithin = new HashSet<Collider>();

    /// <summary>
    /// When object enters trigger
    /// </summary>
    public event Action<Transform> TriggerEntered;
    /// <summary>
    /// Each frame, for each object within trigger
    /// </summary>
    public event Action<Transform> TriggerStay;
    /// <summary>
    /// When object leaves trigger
    /// </summary>
    public event Action<Transform> TriggerLeft; 
    
    protected override void Start()
    {
        base.Start();

        if ( ~layers.value == 0 )
        {
            checkString = true;
            if ( tagToCheck == "None" )
                checkNone = true;
        }
    }

    /// <summary>
    /// Test whether <paramref name="toTest"/> is inside trigger
    /// </summary>
    /// <param name="toTest"></param>
    /// <returns></returns>
    public bool IsColliderWithin( Collider toTest )
    {
        return within.Contains( toTest );
    }

    public bool IsColliderWithin() {
        return within.Count > 0;
    }
    
    public int NumCollidersWithin()
    {
        return within.Count;
    }
    /// <summary>
    /// Get enumerable list of colliders within trigger
    /// </summary>
    /// <returns></returns>
    public IEnumerable<Collider> GetCollidersEnumerable()
    {
        if ( !saveCollidersWithin )
        {
            DebugExtras.LogWarning( "Trigger not saving colliders" );
            yield break;
        }

        tempWithin.Clear();
        foreach ( var coll in within.EnumerateRemoveNull() ) {
            if ( coll )
                yield return coll;
            else
                tempWithin.Add( coll );
        }
        foreach ( var toRemove in tempWithin )
            within.Remove( toRemove );
    }
    
    void OnTriggerEnter( Collider other )
    {   
        if ( checkNone || ( checkString && other.CompareTag( tagToCheck ) ) || ( ( 1 << other.gameObject.layer ) & layers.value ) != 0 )
        {
            if ( saveCollidersWithin )
                within.AddUnique( other );

            if ( TriggerEntered != null )
            {
                TriggerEntered( other.transform );
            }
        }
    }
    
    void OnTriggerStay( Collider other )
    {
        // only for layers
        if ( checkNone || ( ( 1 << other.gameObject.layer ) & layers.value ) != 0 )
            if ( TriggerStay != null ) TriggerStay( other.transform );
    }
    
    void OnTriggerExit( Collider other )
    {
        if ( checkNone || ( checkString && other.CompareTag( tagToCheck ) ) || ( ( 1 << other.gameObject.layer ) & layers.value ) != 0 )
        {
            if ( saveCollidersWithin )
                within.Remove( other );

            if ( TriggerLeft != null )
                TriggerLeft( other.transform );
        }
    }
}
