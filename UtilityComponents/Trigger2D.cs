/* Trigger.cs
 * ----------------------------
 * Copyright Eddie Cameron & Grasshopper, 2014
 * Open-sourced under the MIT licence
 * ----------------------------
 * 
 */

using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

/// <summary>
/// Drop on a 2D trigger collider to have it send events when specified layers enter/stay/exit.
/// </summary>
/// <remarks>
/// Useful for triggers on child objects and for making dealing with layer mask problems far easier
/// </remarks>
[RequireComponent( typeof( Collider2D ) )]
public class Trigger2D : BehaviourBase {
    /// <summary>
    /// What layers are trigger activated by
    /// </summary>
    public LayerMask layers; // Much faster, overrides tag string if set
    /// <summary>
    /// Does the trigger keep track of colliders within itself?
    /// </summary>
    public bool saveCollidersWithin;
    /// <summary>
    /// Which tags activate this trigger
    /// </summary>
    /// <remarks>Layers is much faster</remarks>
    public string tagToCheck = "None";

    bool checkNone;
    bool checkString;
    HashSet<Collider2D> within = new HashSet<Collider2D>();

    /// <summary>
    /// When object enters trigger
    /// </summary>
    public event Action<Transform> TriggerEntered;
    /// <summary>
    /// Each frame, for each object within trigger
    /// </summary>
    public event Action<Transform> TriggerStay;
    /// <summary>
    /// When object leaves trigger
    /// </summary>
    public event Action<Transform> TriggerLeft; 
    
    protected override void Start() {
        base.Start();

        if ( ~layers.value == 0 ) {
            checkString = true;
            if ( tagToCheck == "None" )
                checkNone = true;
        }
    }

    /// <summary>
    /// Get array of all colliders currently inside trigger
    /// </summary>
    /// <returns></returns>
    public Collider2D[] GetCollidersWithin() {
        if ( !saveCollidersWithin ) {
            DebugExtras.LogWarning( "Trigger not saving colliders" );
            return new Collider2D[0];
        }

        var colliders = new Collider2D[within.Count];
        within.CopyTo( colliders );
        return colliders;
    }

    /// <summary>
    /// Test whether <paramref name="toTest"/> is inside trigger
    /// </summary>
    /// <param name="toTest"></param>
    /// <returns></returns>
    public bool IsColliderWithin( Collider2D toTest )
    {
        return within.Contains( toTest );
    }

    
    
    public int NumCollidersWithin()
    {
        return within.Count;
    }
    /// <summary>
    /// Get enumerable list of colliders within trigger
    /// </summary>
    /// <returns></returns>
    public IEnumerable<Collider2D> GetCollidersEnumerable()
    {
        if ( !saveCollidersWithin )
        {
            DebugExtras.LogWarning( "Trigger not saving colliders" );
            yield break;
        }
        foreach ( var coll in within.EnumerateRemoveNull() )
            yield return coll;
    }
    
    void OnTriggerEnter2D( Collider2D other )
    {   
        if ( checkNone || ( checkString && other.CompareTag( tagToCheck ) ) || ( ( 1 << other.gameObject.layer ) & layers.value ) != 0 )
        {
            if ( saveCollidersWithin )
                within.AddUnique( other );

            if ( TriggerEntered != null )
            {
                TriggerEntered( other.transform );
            }
        }
    }
    
    void OnTriggerStay2D( Collider2D other )
    {
        // only for layers
        if ( checkNone || ( ( 1 << other.gameObject.layer ) & layers.value ) != 0 )
            if ( TriggerStay != null ) TriggerStay( other.transform );
    }
    
    void OnTriggerExit2D( Collider2D other )
    {
        if ( checkNone || ( checkString && other.CompareTag( tagToCheck ) ) || ( ( 1 << other.gameObject.layer ) & layers.value ) != 0 )
        {
            if ( saveCollidersWithin )
                within.Remove( other );

            if ( TriggerLeft != null )
                TriggerLeft( other.transform );
        }
    }
}
