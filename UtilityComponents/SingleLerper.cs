﻿/* SingleLerper.cs
 * Copyright Eddie Cameron 2013
 * ----------------------------
 *
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SingleLerper : BehaviourBase 
{
    [SerializeField]
    float _curValue;
    public float lerpSpeed = 1f; // how far can value change in 1 sec

    public float curValue { get; private set; }

    public float targetValue { get; private set; }
    float curLerpAmount = 1f;
    float lastValue;

    protected override void Awake() {
        base.Awake();

        lastValue = curValue = _curValue;
    }

    protected override void Update() {
        base.Update();

        if ( curLerpAmount < 1f && targetValue != lastValue ) {
            curValue = Mathf.Lerp( lastValue, targetValue, curLerpAmount );
            curLerpAmount += Time.deltaTime * lerpSpeed / Mathf.Abs( targetValue - lastValue );
        }
        else {
            curValue = targetValue;
            enabled = false;
        }
    }

    public void SetTarget( float lerpTarget ) {
        if ( lerpTarget != targetValue ) {
            targetValue = lerpTarget;
            lastValue = curValue;
            curLerpAmount = Time.deltaTime * lerpSpeed / Mathf.Abs( targetValue - lastValue );
            enabled = true;
        }
    }

    public void SetValue( float newValue ) {
        if ( newValue != curValue ) {
            curValue = lastValue = newValue;
            curLerpAmount = Time.deltaTime * lerpSpeed / Mathf.Abs( targetValue - lastValue );
            enabled = true;
        }
    }

    #region Static Creator
    static Dictionary<GameObject, SingleLerper> lerpers = new Dictionary<GameObject, SingleLerper>();

    public static SingleLerper SetTargetLerp( GameObject onObject, float target ) {
        SingleLerper lerper;
        if ( !lerpers.TryGetValue( onObject, out lerper ) ) {
            lerper = onObject.AddComponent<SingleLerper>();
            lerpers.Add( onObject, lerper );
        }
        lerper.SetTarget( target );

        return lerper;
    }
    #endregion
}