﻿/**
 * PennerDoubleAnimation
 * Animates the value of a double property between two target values using 
 * Robert Penner's easing equations for interpolation over a specified Duration.
 *
 * @author		Darren David darren-code@lookorfeel.com
 * @version		1.0
 *
 * Credit/Thanks:
 * Robert Penner - The easing equations we all know and love 
 *   (http://robertpenner.com/easing/) [See License.txt for license info]
 * 
 * Lee Brimelow - initial port of Penner's equations to WPF 
 *   (http://thewpfblog.com/?p=12)
 * 
 * Zeh Fernando - additional equations (out/in) from 
 *   caurina.transitions.Tweener (http://code.google.com/p/tweener/)
 *   [See License.txt for license info]
 */

using System;

namespace MathUtils {
    public class Easer {
        /// <summary>
        /// Enumeration of all easing equations.
        /// </summary>
        public enum Equations {
            Linear,
            QuadEaseOut,
            QuadEaseIn,
            QuadEaseInOut,
            QuadEaseOutIn,
            ExpoEaseOut,
            ExpoEaseIn,
            ExpoEaseInOut,
            ExpoEaseOutIn,
            CubicEaseOut,
            CubicEaseIn,
            CubicEaseInOut,
            CubicEaseOutIn,
            QuartEaseOut,
            QuartEaseIn,
            QuartEaseInOut,
            QuartEaseOutIn,
            QuintEaseOut,
            QuintEaseIn,
            QuintEaseInOut,
            QuintEaseOutIn,
            CircEaseOut,
            CircEaseIn,
            CircEaseInOut,
            CircEaseOutIn,
            SineEaseOut,
            SineEaseIn,
            SineEaseInOut,
            SineEaseOutIn,
            ElasticEaseOut,
            ElasticEaseIn,
            ElasticEaseInOut,
            ElasticEaseOutIn,
            BounceEaseOut,
            BounceEaseIn,
            BounceEaseInOut,
            BounceEaseOutIn,
            BackEaseOut,
            BackEaseIn,
            BackEaseInOut,
            BackEaseOutIn
        }

        #region Equations

        /// <summary>
        /// Easing equation function for given ease type
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double DoEquation( double t, double b, double c, double d, Equations easeType ) {
            switch ( easeType ) {
            case Equations.Linear:
                return Linear( t, b, c, d );
                
            case Equations.ExpoEaseOut:
                return ExpoEaseOut( t, b, c, d );
                
            case Equations.ExpoEaseIn:
                return ExpoEaseIn( t, b, c, d );
                
            case Equations.ExpoEaseOutIn:
                return ExpoEaseOutIn( t, b, c, d );
                
            case Equations.ExpoEaseInOut:
                return ExpoEaseInOut( t, b, c, d );
                
            case Equations.CircEaseOut:
                return CircEaseOut( t, b, c, d );
                
            case Equations.CircEaseIn:
                return CircEaseIn( t, b, c, d );
                
            case Equations.CircEaseOutIn:
                return CircEaseOutIn( t, b, c, d );
                
            case Equations.CircEaseInOut:
                return CircEaseInOut( t, b, c, d );
                
            case Equations.QuadEaseOut:
                return QuadEaseOut( t, b, c, d );
                
            case Equations.QuadEaseIn:
                return QuadEaseIn( t, b, c, d );
                
            case Equations.QuadEaseOutIn:
                return QuadEaseOutIn( t, b, c, d );
                
            case Equations.QuadEaseInOut:
                return QuadEaseInOut( t, b, c, d );
                
            case Equations.SineEaseOut:
                return SineEaseOut( t, b, c, d );
                
            case Equations.SineEaseIn:
                return SineEaseIn( t, b, c, d );
                
            case Equations.SineEaseOutIn:
                return SineEaseOutIn( t, b, c, d );
                
            case Equations.SineEaseInOut:
                return SineEaseInOut( t, b, c, d );
                
            case Equations.CubicEaseOut:
                return CubicEaseOut( t, b, c, d );
                
            case Equations.CubicEaseIn:
                return CubicEaseIn( t, b, c, d );
                
            case Equations.CubicEaseOutIn:
                return CubicEaseOutIn( t, b, c, d );
                
            case Equations.CubicEaseInOut:
                return CubicEaseInOut( t, b, c, d );
                
            case Equations.QuartEaseOut:
                return QuartEaseOut( t, b, c, d );
                
            case Equations.QuartEaseIn:
                return QuartEaseIn( t, b, c, d );
                
            case Equations.QuartEaseOutIn:
                return QuartEaseOutIn( t, b, c, d );
                
            case Equations.QuartEaseInOut:
                return QuartEaseInOut( t, b, c, d );
                
            case Equations.QuintEaseOut:
                return QuintEaseOut( t, b, c, d );
                
            case Equations.QuintEaseIn:
                return QuintEaseIn( t, b, c, d );
                
            case Equations.QuintEaseOutIn:
                return QuintEaseOutIn( t, b, c, d );
                
            case Equations.QuintEaseInOut:
                return QuintEaseInOut( t, b, c, d );
                
            case Equations.ElasticEaseOut:
                return ElasticEaseOut( t, b, c, d );
                
            case Equations.ElasticEaseIn:
                return ElasticEaseIn( t, b, c, d );
                
            case Equations.ElasticEaseOutIn:
                return ElasticEaseOutIn( t, b, c, d );
                
            case Equations.ElasticEaseInOut:
                return ElasticEaseInOut( t, b, c, d );
                
            case Equations.BounceEaseOut:
                return BounceEaseOut( t, b, c, d );
                
            case Equations.BounceEaseIn:
                return BounceEaseIn( t, b, c, d );
                
            case Equations.BounceEaseOutIn:
                return BounceEaseOutIn( t, b, c, d );
                
            case Equations.BounceEaseInOut:
                return BounceEaseInOut( t, b, c, d );
                
            case Equations.BackEaseOut:
                return BackEaseOut( t, b, c, d );
                
            case Equations.BackEaseIn:
                return BackEaseIn( t, b, c, d );
                
            case Equations.BackEaseOutIn:
                return BackEaseOutIn( t, b, c, d );
                
            case Equations.BackEaseInOut:
                return BackEaseInOut( t, b, c, d );
             
            default:
                DebugExtras.LogWarning( "Equation " + easeType + " not recognised" );
                return Linear( t, b, c, d );
            }
        }

        /// <summary>
        /// Easing equation function for given ease type
        /// </summary>
        /// <param name="t">Lerp time</param>
        /// <returns>The correct value.</returns>
        public static double DoEquation( double t, Equations easeType ) {
            switch ( easeType ) {
            case Equations.Linear:
                return Linear( t );

            case Equations.ExpoEaseOut:
                return ExpoEaseOut( t );

            case Equations.ExpoEaseIn:
                return ExpoEaseIn( t );

            case Equations.ExpoEaseOutIn:
                return ExpoEaseOutIn( t );

            case Equations.ExpoEaseInOut:
                return ExpoEaseInOut( t );

            case Equations.CircEaseOut:
                return CircEaseOut( t );

            case Equations.CircEaseIn:
                return CircEaseIn( t );

            case Equations.CircEaseOutIn:
                return CircEaseOutIn( t );

            case Equations.CircEaseInOut:
                return CircEaseInOut( t );

            case Equations.QuadEaseOut:
                return QuadEaseOut( t );

            case Equations.QuadEaseIn:
                return QuadEaseIn( t );

            case Equations.QuadEaseOutIn:
                return QuadEaseOutIn( t );

            case Equations.QuadEaseInOut:
                return QuadEaseInOut( t );

            case Equations.SineEaseOut:
                return SineEaseOut( t );

            case Equations.SineEaseIn:
                return SineEaseIn( t );

            case Equations.SineEaseOutIn:
                return SineEaseOutIn( t );

            case Equations.SineEaseInOut:
                return SineEaseInOut( t );

            case Equations.CubicEaseOut:
                return CubicEaseOut( t );

            case Equations.CubicEaseIn:
                return CubicEaseIn( t );

            case Equations.CubicEaseOutIn:
                return CubicEaseOutIn( t );

            case Equations.CubicEaseInOut:
                return CubicEaseInOut( t );

            case Equations.QuartEaseOut:
                return QuartEaseOut( t );

            case Equations.QuartEaseIn:
                return QuartEaseIn( t );

            case Equations.QuartEaseOutIn:
                return QuartEaseOutIn( t );

            case Equations.QuartEaseInOut:
                return QuartEaseInOut( t );

            case Equations.QuintEaseOut:
                return QuintEaseOut( t );

            case Equations.QuintEaseIn:
                return QuintEaseIn( t );

            case Equations.QuintEaseOutIn:
                return QuintEaseOutIn( t );

            case Equations.QuintEaseInOut:
                return QuintEaseInOut( t );

            case Equations.ElasticEaseOut:
                return ElasticEaseOut( t );

            case Equations.ElasticEaseIn:
                return ElasticEaseIn( t );

            case Equations.ElasticEaseOutIn:
                return ElasticEaseOutIn( t );

            case Equations.ElasticEaseInOut:
                return ElasticEaseInOut( t );

            case Equations.BounceEaseOut:
                return BounceEaseOut( t );

            case Equations.BounceEaseIn:
                return BounceEaseIn( t );

            case Equations.BounceEaseOutIn:
                return BounceEaseOutIn( t );

            case Equations.BounceEaseInOut:
                return BounceEaseInOut( t );

            case Equations.BackEaseOut:
                return BackEaseOut( t );

            case Equations.BackEaseIn:
                return BackEaseIn( t );

            case Equations.BackEaseOutIn:
                return BackEaseOutIn( t );

            case Equations.BackEaseInOut:
                return BackEaseInOut( t );

            default:
                DebugExtras.LogWarning( "Equation " + easeType + " not recognised" );
                return Linear( t );
            }
        }

        #region Linear

        /// <summary>
        /// Easing equation function for a simple linear tweening, with no easing.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double Linear( double t, double b, double c, double d ) {
            return c * t / d + b;
        }

        /// <summary>
        /// Easing equation function for a simple linear tweening, with no easing.
        /// </summary>
        /// <param name="l">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double Linear( double l ) {
            return l;
        }
        #endregion

        #region Expo

        /// <summary>
        /// Easing equation function for an exponential (2^t) easing out: 
        /// decelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double ExpoEaseOut( double t, double b, double c, double d ) {
            return ( t == d ) ? b + c : c * ( -Math.Pow( 2, -10 * t / d ) + 1 ) + b;
        }

        /// <summary>
        /// Easing equation function for an exponential (2^t) easing out: 
        /// decelerating from zero velocity.
        /// </summary>
        /// <param name="l">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double ExpoEaseOut( double l ) {
            return ( l == 1.0 ) ? l : -Math.Pow( 2, -10 * l ) + 1;
        }

        /// <summary>
        /// Easing equation function for an exponential (2^t) easing in: 
        /// accelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double ExpoEaseIn( double t, double b, double c, double d ) {
            return ( t == 0 ) ? b : c * Math.Pow( 2, 10 * ( t / d - 1 ) ) + b;
        }

        /// <summary>
        /// Easing equation function for an exponential (2^t) easing in: 
        /// accelerating from zero velocity.
        /// </summary>
        /// <param name="l">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double ExpoEaseIn( double l ) {
            return ( l == 0 ) ? 0 : Math.Pow( 2, 10 * ( l - 1.0 ) );
        }

        /// <summary>
        /// Easing equation function for an exponential (2^t) easing in/out: 
        /// acceleration until halfway, then deceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double ExpoEaseInOut( double t, double b, double c, double d ) {
            if ( t == 0 )
                return b;

            if ( t == d )
                return b + c;

            if ( ( t /= d / 2 ) < 1 )
                return c / 2 * Math.Pow( 2, 10 * ( t - 1 ) ) + b;

            return c / 2 * ( -Math.Pow( 2, -10 * --t ) + 2 ) + b;
        }

        /// <summary>
        /// Easing equation function for an exponential (2^t) easing in/out: 
        /// acceleration until halfway, then deceleration.
        /// </summary>
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double ExpoEaseInOut( double t ) {
            if ( t == 0 )
                return t;

            if ( t == 1 )
                return 1;

            if ( ( t *= 2 ) < 1 )
                return .5 * Math.Pow( 2, 10 * ( t - 1 ) );

            return .5 * ( -Math.Pow( 2, -10 * --t ) + 2 );
        }

        /// <summary>
        /// Easing equation function for an exponential (2^t) easing out/in: 
        /// deceleration until halfway, then acceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double ExpoEaseOutIn( double t, double b, double c, double d ) {
            if ( t < d / 2 )
                return ExpoEaseOut( t * 2, b, c / 2, d );

            return ExpoEaseIn( ( t * 2 ) - d, b + c / 2, c / 2, d );
        }

        /// <summary>
        /// Easing equation function for an exponential (2^t) easing out/in: 
        /// deceleration until halfway, then acceleration.
        /// </summary>       
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double ExpoEaseOutIn( double t ) {
            if ( t < .5 )
                return ExpoEaseOut( t * 2, 0, .5, 1 );

            return ExpoEaseIn( ( t * 2 ) - 1, .5, .5, 1 );
        }
        #endregion

        #region Circular

        /// <summary>
        /// Easing equation function for a circular (sqrt(1-t^2)) easing out: 
        /// decelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double CircEaseOut( double t, double b, double c, double d ) {
            return c * Math.Sqrt( 1 - ( t = t / d - 1 ) * t ) + b;
        }

        /// <summary>
        /// Easing equation function for a circular (sqrt(1-t^2)) easing out: 
        /// decelerating from zero velocity.
        /// </summary>
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double CircEaseOut( double t ) {
            return Math.Sqrt( 1 - ( t = t - 1 ) * t );
        }

        /// <summary>
        /// Easing equation function for a circular (sqrt(1-t^2)) easing in: 
        /// accelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double CircEaseIn( double t, double b, double c, double d ) {
            return -c * ( Math.Sqrt( 1 - ( t /= d ) * t ) - 1 ) + b;
        }

        /// <summary>
        /// Easing equation function for a circular (sqrt(1-t^2)) easing in: 
        /// accelerating from zero velocity.
        /// </summary>
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double CircEaseIn( double t ) {
            if ( t > 1 )
                t = 1;
            else if ( t < 0 )
                t = 0;
            return -( Math.Sqrt( 1 - t * t ) - 1 );
        }

        /// <summary>
        /// Easing equation function for a circular (sqrt(1-t^2)) easing in/out: 
        /// acceleration until halfway, then deceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double CircEaseInOut( double t, double b, double c, double d ) {
            if ( ( t /= d / 2 ) < 1 )
                return -c / 2 * ( Math.Sqrt( 1 - t * t ) - 1 ) + b;

            return c / 2 * ( Math.Sqrt( 1 - ( t -= 2 ) * t ) + 1 ) + b;
        }

        /// <summary>
        /// Easing equation function for a circular (sqrt(1-t^2)) easing in/out: 
        /// acceleration until halfway, then deceleration.
        /// </summary>
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double CircEaseInOut( double t ) {
            if ( ( t *= 2 ) < 1 )
                return -.5 * ( Math.Sqrt( 1 - t * t ) - 1 );

            return .5 * ( Math.Sqrt( 1 - ( t -= 2 ) * t ) + 1 );
        }

        /// <summary>
        /// Easing equation function for a circular (sqrt(1-t^2)) easing in/out: 
        /// acceleration until halfway, then deceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double CircEaseOutIn( double t, double b, double c, double d ) {
            if ( t < d / 2 )
                return CircEaseOut( t * 2, b, c / 2, d );

            return CircEaseIn( ( t * 2 ) - d, b + c / 2, c / 2, d );
        }

        /// <summary>
        /// Easing equation function for a circular (sqrt(1-t^2)) easing in/out: 
        /// acceleration until halfway, then deceleration.
        /// </summary>
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double CircEaseOutIn( double t ) {
            if ( t < .5 )
                return CircEaseOut( t * 2, 0, .5, 1 );

            return CircEaseIn( ( t * 2 ) - 1, .5, .5, 1 );
        }
        #endregion

        #region Quad

        /// <summary>
        /// Easing equation function for a quadratic (t^2) easing out: 
        /// decelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double QuadEaseOut( double t, double b, double c, double d ) {
            return -c * ( t /= d ) * ( t - 2 ) + b;
        }

        /// <summary>
        /// Easing equation function for a quadratic (t^2) easing out: 
        /// decelerating from zero velocity.
        /// </summary>
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double QuadEaseOut( double t ) {
            return -t * ( t - 2 );
        }

        /// <summary>
        /// Easing equation function for a quadratic (t^2) easing in: 
        /// accelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double QuadEaseIn( double t, double b, double c, double d ) {
            return c * ( t /= d ) * t + b;
        }

        /// <summary>
        /// Easing equation function for a quadratic (t^2) easing in: 
        /// accelerating from zero velocity.
        /// </summary>
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double QuadEaseIn( double t ) {
            return t * t;
        }

        /// <summary>
        /// Easing equation function for a quadratic (t^2) easing in/out: 
        /// acceleration until halfway, then deceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double QuadEaseInOut( double t, double b, double c, double d ) {
            if ( ( t /= d / 2 ) < 1 )
                return c / 2 * t * t + b;

            return -c / 2 * ( ( --t ) * ( t - 2 ) - 1 ) + b;
        }

        /// <summary>
        /// Easing equation function for a quadratic (t^2) easing in/out: 
        /// acceleration until halfway, then deceleration.
        /// </summary>
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double QuadEaseInOut( double t ) {
            if ( ( t *= 2 ) < 1 )
                return .5 * t * t;

            return -.5 * ( ( --t ) * ( t - 2 ) - 1 );
        }

        /// <summary>
        /// Easing equation function for a quadratic (t^2) easing out/in: 
        /// deceleration until halfway, then acceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double QuadEaseOutIn( double t, double b, double c, double d ) {
            if ( t < d / 2 )
                return QuadEaseOut( t * 2, b, c / 2, d );

            return QuadEaseIn( ( t * 2 ) - d, b + c / 2, c / 2, d );
        }

        /// <summary>
        /// Easing equation function for a quadratic (t^2) easing out/in: 
        /// deceleration until halfway, then acceleration.
        /// </summary>
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double QuadEaseOutIn( double t ) {
            if ( t < .5 )
                return QuadEaseOut( t * 2 ) * .5;

            return .5 + QuadEaseIn( ( t * 2 ) - 1 ) * .5;
        }
        #endregion

        #region Sine

        /// <summary>
        /// Easing equation function for a sinusoidal (sin(t)) easing out: 
        /// decelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double SineEaseOut( double t, double b, double c, double d ) {
            return c * Math.Sin( t / d * ( Math.PI / 2 ) ) + b;
        }

        /// <summary>
        /// Easing equation function for a sinusoidal (sin(t)) easing out: 
        /// decelerating from zero velocity.
        /// </summary>
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double SineEaseOut( double t ) {
            return Math.Sin( t * ( Math.PI / 2 ) );
        }

        /// <summary>
        /// Easing equation function for a sinusoidal (sin(t)) easing in: 
        /// accelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double SineEaseIn( double t, double b, double c, double d ) {
            return -c * Math.Cos( t / d * ( Math.PI / 2 ) ) + c + b;
        }

        /// <summary>
        /// Easing equation function for a sinusoidal (sin(t)) easing in: 
        /// accelerating from zero velocity.
        /// </summary>
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double SineEaseIn( double t ) {
            return -Math.Cos( t * ( Math.PI / 2 ) ) + 1;
        }

        /// <summary>
        /// Easing equation function for a sinusoidal (sin(t)) easing in/out: 
        /// acceleration until halfway, then deceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double SineEaseInOut( double t, double b, double c, double d ) {
            if ( ( t /= d / 2 ) < 1 )
                return c / 2 * ( Math.Sin( Math.PI * t / 2 ) ) + b;

            return -c / 2 * ( Math.Cos( Math.PI * --t / 2 ) - 2 ) + b;
        }

        /// <summary>
        /// Easing equation function for a sinusoidal (sin(t)) easing in/out: 
        /// acceleration until halfway, then deceleration.
        /// </summary>
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double SineEaseInOut( double t ) {
            if ( ( t *= 2 ) < 1 )
                return .5 * ( Math.Sin( Math.PI * t / 2 ) );

            return -.5 * ( Math.Cos( Math.PI * --t / 2 ) - 2 );
        }

        /// <summary>
        /// Easing equation function for a sinusoidal (sin(t)) easing in/out: 
        /// deceleration until halfway, then acceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double SineEaseOutIn( double t, double b, double c, double d ) {
            if ( t < d / 2 )
                return SineEaseOut( t * 2, b, c / 2, d );

            return SineEaseIn( ( t * 2 ) - d, b + c / 2, c / 2, d );
        }

        /// <summary>
        /// Easing equation function for a sinusoidal (sin(t)) easing in/out: 
        /// deceleration until halfway, then acceleration.
        /// </summary>
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double SineEaseOutIn( double t ) {
            if ( t < .5 )
                return SineEaseOut( t * 2 ) * .5;

            return .5 + SineEaseIn( ( t * 2 ) - 1 ) * .5;
        }
        #endregion

        #region Cubic

        /// <summary>
        /// Easing equation function for a cubic (t^3) easing out: 
        /// decelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double CubicEaseOut( double t, double b, double c, double d ) {
            return c * ( ( t = t / d - 1 ) * t * t + 1 ) + b;
        }

        /// <summary>
        /// Easing equation function for a cubic (t^3) easing out: 
        /// decelerating from zero velocity.
        /// </summary>
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double CubicEaseOut( double t ) {
            return ( ( t = t - 1 ) * t * t + 1 );
        }

        /// <summary>
        /// Easing equation function for a cubic (t^3) easing in: 
        /// accelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double CubicEaseIn( double t, double b, double c, double d ) {
            return c * ( t /= d ) * t * t + b;
        }

        /// <summary>
        /// Easing equation function for a cubic (t^3) easing in: 
        /// accelerating from zero velocity.
        /// </summary>
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double CubicEaseIn( double t ) {
            return t * t * t;
        }

        /// <summary>
        /// Easing equation function for a cubic (t^3) easing in/out: 
        /// acceleration until halfway, then deceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double CubicEaseInOut( double t, double b, double c, double d ) {
            if ( ( t /= d / 2 ) < 1 )
                return c / 2 * t * t * t + b;

            return c / 2 * ( ( t -= 2 ) * t * t + 2 ) + b;
        }

        /// <summary>
        /// Easing equation function for a cubic (t^3) easing in/out: 
        /// acceleration until halfway, then deceleration.
        /// </summary>
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double CubicEaseInOut( double t ) {
            if ( ( t *= 2 ) < 1 )
                return .5 * t * t * t;

            return .5 * ( ( t -= 2 ) * t * t + 2 );
        }

        /// <summary>
        /// Easing equation function for a cubic (t^3) easing out/in: 
        /// deceleration until halfway, then acceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double CubicEaseOutIn( double t, double b, double c, double d ) {
            if ( t < d / 2 )
                return CubicEaseOut( t * 2, b, c / 2, d );

            return CubicEaseIn( ( t * 2 ) - d, b + c / 2, c / 2, d );
        }

        /// <summary>
        /// Easing equation function for a cubic (t^3) easing out/in: 
        /// deceleration until halfway, then acceleration.
        /// </summary>
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double CubicEaseOutIn( double t ) {
            if ( t < .5 )
                return CubicEaseOut( t * 2 ) * .5;

            return .5 + CubicEaseIn( ( t * 2 ) - 1 ) * .5;
        }
        #endregion

        #region Quartic

        /// <summary>
        /// Easing equation function for a quartic (t^4) easing out: 
        /// decelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double QuartEaseOut( double t, double b, double c, double d ) {
            return -c * ( ( t = t / d - 1 ) * t * t * t - 1 ) + b;
        }

        /// <summary>
        /// Easing equation function for a quartic (t^4) easing out: 
        /// decelerating from zero velocity.
        /// </summary>
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double QuartEaseOut( double t ) {
            return -( ( t = t - 1 ) * t * t * t - 1 );
        }

        /// <summary>
        /// Easing equation function for a quartic (t^4) easing in: 
        /// accelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double QuartEaseIn( double t, double b, double c, double d ) {
            return c * ( t /= d ) * t * t * t + b;
        }

        /// <summary>
        /// Easing equation function for a quartic (t^4) easing in: 
        /// accelerating from zero velocity.
        /// </summary>
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double QuartEaseIn( double t ) {
            return t * t * t * t;
        }

        /// <summary>
        /// Easing equation function for a quartic (t^4) easing in/out: 
        /// acceleration until halfway, then deceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double QuartEaseInOut( double t, double b, double c, double d ) {
            if ( ( t /= d / 2 ) < 1 )
                return c / 2 * t * t * t * t + b;

            return -c / 2 * ( ( t -= 2 ) * t * t * t - 2 ) + b;
        }

        /// <summary>
        /// Easing equation function for a quartic (t^4) easing in/out: 
        /// acceleration until halfway, then deceleration.
        /// </summary>
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double QuartEaseInOut( double t ) {
            if ( ( t *= 2 ) < 1 )
                return .5 * t * t * t * t;

            return -.5 * ( ( t -= 2 ) * t * t * t - 2 );
        }

        /// <summary>
        /// Easing equation function for a quartic (t^4) easing out/in: 
        /// deceleration until halfway, then acceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double QuartEaseOutIn( double t, double b, double c, double d ) {
            if ( t < d / 2 )
                return QuartEaseOut( t * 2, b, c / 2, d );

            return QuartEaseIn( ( t * 2 ) - d, b + c / 2, c / 2, d );
        }

        /// <summary>
        /// Easing equation function for a quartic (t^4) easing out/in: 
        /// deceleration until halfway, then acceleration.
        /// </summary>
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double QuartEaseOutIn( double t ) {
            if ( t < .5 )
                return QuartEaseOut( t * 2 ) * .5;

            return .5 + QuartEaseIn( ( t * 2 ) - 1 ) * .5;
        }
        #endregion

        #region Quintic

        /// <summary>
        /// Easing equation function for a quintic (t^5) easing out: 
        /// decelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double QuintEaseOut( double t, double b, double c, double d ) {
            return c * ( ( t = t / d - 1 ) * t * t * t * t + 1 ) + b;
        }

        /// <summary>
        /// Easing equation function for a quintic (t^5) easing out: 
        /// decelerating from zero velocity.
        /// </summary>
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double QuintEaseOut( double t ) {
            return ( t = t - 1 ) * t * t * t * t + 1;
        }

        /// <summary>
        /// Easing equation function for a quintic (t^5) easing in: 
        /// accelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double QuintEaseIn( double t, double b, double c, double d ) {
            return c * ( t /= d ) * t * t * t * t + b;
        }

        /// <summary>
        /// Easing equation function for a quintic (t^5) easing in: 
        /// accelerating from zero velocity.
        /// </summary>
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double QuintEaseIn( double t ) {
            return t * t * t * t * t;
        }

        /// <summary>
        /// Easing equation function for a quintic (t^5) easing in/out: 
        /// acceleration until halfway, then deceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double QuintEaseInOut( double t, double b, double c, double d ) {
            if ( ( t /= d / 2 ) < 1 )
                return c / 2 * t * t * t * t * t + b;
            return c / 2 * ( ( t -= 2 ) * t * t * t * t + 2 ) + b;
        }

        /// <summary>
        /// Easing equation function for a quintic (t^5) easing in/out: 
        /// acceleration until halfway, then deceleration.
        /// </summary>
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double QuintEaseInOut( double t ) {
            if ( ( t *= 2 ) < 1 )
                return .5 * t * t * t * t * t;
            return .5 * ( ( t -= 2 ) * t * t * t * t + 2 );
        }

        /// <summary>
        /// Easing equation function for a quintic (t^5) easing in/out: 
        /// acceleration until halfway, then deceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double QuintEaseOutIn( double t, double b, double c, double d ) {
            if ( t < d / 2 )
                return QuintEaseOut( t * 2, b, c / 2, d );
            return QuintEaseIn( ( t * 2 ) - d, b + c / 2, c / 2, d );
        }

        /// <summary>
        /// Easing equation function for a quintic (t^5) easing in/out: 
        /// acceleration until halfway, then deceleration.
        /// </summary>
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double QuintEaseOutIn( double t ) {
            if ( t < .5 )
                return QuintEaseOut( t * 2 ) * .5;
            return .5 + QuintEaseIn( ( t * 2 ) - 1 ) * .5;
        }

        #endregion

        #region Elastic

        /// <summary>
        /// Easing equation function for an elastic (exponentially decaying sine wave) easing out: 
        /// decelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double ElasticEaseOut( double t, double b, double c, double d ) {
            if ( ( t /= d ) == 1 )
                return b + c;

            double p = d * .3;
            double s = p / 4;

            return ( c * Math.Pow( 2, -10 * t ) * Math.Sin( ( t * d - s ) * ( 2 * Math.PI ) / p ) + c + b );
        }

        /// <summary>
        /// Easing equation function for an elastic (exponentially decaying sine wave) easing out: 
        /// decelerating from zero velocity.
        /// </summary>
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double ElasticEaseOut( double t ) {
            if ( t >= 1 )
                return 1;

            double p = .3;
            double s = p / 4;

            return Math.Pow( 2, -10 * t ) * Math.Sin( ( t - s ) * ( 2 * Math.PI ) / p ) + 1;
        }

        /// <summary>
        /// Easing equation function for an elastic (exponentially decaying sine wave) easing in: 
        /// accelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double ElasticEaseIn( double t, double b, double c, double d ) {
            if ( ( t /= d ) == 1 )
                return b + c;

            double p = d * .3;
            double s = p / 4;

            return -( c * Math.Pow( 2, 10 * ( t -= 1 ) ) * Math.Sin( ( t * d - s ) * ( 2 * Math.PI ) / p ) ) + b;
        }

        /// <summary>
        /// Easing equation function for an elastic (exponentially decaying sine wave) easing in: 
        /// accelerating from zero velocity.
        /// </summary>
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double ElasticEaseIn( double t ) {
            if ( t >= 1 )
                return 1;

            double p = .3;
            double s = p / 4;

            return -Math.Pow( 2, 10 * ( t -= 1 ) ) * Math.Sin( ( t - s ) * ( 2 * Math.PI ) / p );
        }

        /// <summary>
        /// Easing equation function for an elastic (exponentially decaying sine wave) easing in/out: 
        /// acceleration until halfway, then deceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double ElasticEaseInOut( double t, double b, double c, double d ) {
            if ( ( t /= d / 2 ) == 2 )
                return b + c;

            double p = d * ( .3 * 1.5 );
            double s = p / 4;

            if ( t < 1 )
                return -.5 * ( c * Math.Pow( 2, 10 * ( t -= 1 ) ) * Math.Sin( ( t * d - s ) * ( 2 * Math.PI ) / p ) ) + b;
            return c * Math.Pow( 2, -10 * ( t -= 1 ) ) * Math.Sin( ( t * d - s ) * ( 2 * Math.PI ) / p ) * .5 + c + b;
        }

        /// <summary>
        /// Easing equation function for an elastic (exponentially decaying sine wave) easing in/out: 
        /// acceleration until halfway, then deceleration.
        /// </summary>
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double ElasticEaseInOut( double t ) {
            if ( ( t *= 2 ) >= 2 )
                return 1;

            double p = ( .3 * 1.5 );
            double s = p / 4;

            if ( t < 1 )
                return -.5 * ( Math.Pow( 2, 10 * ( t -= 1 ) ) * Math.Sin( ( t - s ) * ( 2 * Math.PI ) / p ) );
            return Math.Pow( 2, -10 * ( t -= 1 ) ) * Math.Sin( ( t - s ) * ( 2 * Math.PI ) / p ) * .5 + 1;
        }

        /// <summary>
        /// Easing equation function for an elastic (exponentially decaying sine wave) easing out/in: 
        /// deceleration until halfway, then acceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double ElasticEaseOutIn( double t, double b, double c, double d ) {
            if ( t < d / 2 )
                return ElasticEaseOut( t * 2, b, c / 2, d );
            return ElasticEaseIn( ( t * 2 ) - d, b + c / 2, c / 2, d );
        }

        /// <summary>
        /// Easing equation function for an elastic (exponentially decaying sine wave) easing out/in: 
        /// deceleration until halfway, then acceleration.
        /// </summary>
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double ElasticEaseOutIn( double t ) {
            if ( t < .5 )
                return ElasticEaseOut( t * 2 ) * .5;
            return .5 + ElasticEaseIn( ( t * 2 ) - 1 ) * .5;
        }
        #endregion

        #region Bounce

        /// <summary>
        /// Easing equation function for a bounce (exponentially decaying parabolic bounce) easing out: 
        /// decelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double BounceEaseOut( double t, double b, double c, double d ) {
            if ( ( t /= d ) < ( 1 / 2.75 ) )
                return c * ( 7.5625 * t * t ) + b;
            else if ( t < ( 2 / 2.75 ) )
                return c * ( 7.5625 * ( t -= ( 1.5 / 2.75 ) ) * t + .75 ) + b;
            else if ( t < ( 2.5 / 2.75 ) )
                return c * ( 7.5625 * ( t -= ( 2.25 / 2.75 ) ) * t + .9375 ) + b;
            else
                return c * ( 7.5625 * ( t -= ( 2.625 / 2.75 ) ) * t + .984375 ) + b;
        }

        /// <summary>
        /// Easing equation function for a bounce (exponentially decaying parabolic bounce) easing out: 
        /// decelerating from zero velocity.
        /// </summary>
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double BounceEaseOut( double t ) {
            if ( t < ( 1 / 2.75 ) )
                return 7.5625 * t * t;
            else if ( t < 2 / 2.75 )
                return ( 7.5625 * ( t -= ( 1.5 / 2.75 ) ) * t + .75 );
            else if ( t < ( 2.5 / 2.75 ) )
                return ( 7.5625 * ( t -= ( 2.25 / 2.75 ) ) * t + .9375 );
            else
                return ( 7.5625 * ( t -= ( 2.625 / 2.75 ) ) * t + .984375 );
        }

        /// <summary>
        /// Easing equation function for a bounce (exponentially decaying parabolic bounce) easing in: 
        /// accelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double BounceEaseIn( double t, double b, double c, double d ) {
            return c - BounceEaseOut( d - t, 0, c, d ) + b;
        }

        /// <summary>
        /// Easing equation function for a bounce (exponentially decaying parabolic bounce) easing in: 
        /// accelerating from zero velocity.
        /// </summary>
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double BounceEaseIn( double t ) {
            return 1 - BounceEaseOut( 1 - t );
        }

        /// <summary>
        /// Easing equation function for a bounce (exponentially decaying parabolic bounce) easing in/out: 
        /// acceleration until halfway, then deceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double BounceEaseInOut( double t, double b, double c, double d ) {
            if ( t < d / 2 )
                return BounceEaseIn( t * 2, 0, c, d ) * .5 + b;
            else
                return BounceEaseOut( t * 2 - d, 0, c, d ) * .5 + c * .5 + b;
        }

        /// <summary>
        /// Easing equation function for a bounce (exponentially decaying parabolic bounce) easing in/out: 
        /// acceleration until halfway, then deceleration.
        /// </summary>
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double BounceEaseInOut( double t ) {
            if ( t < .5 )
                return BounceEaseIn( t * 2 ) * .5;
            else
                return .5 + BounceEaseOut( t * 2 - 1 ) * .5;
        }

        /// <summary>
        /// Easing equation function for a bounce (exponentially decaying parabolic bounce) easing out/in: 
        /// deceleration until halfway, then acceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double BounceEaseOutIn( double t, double b, double c, double d ) {
            if ( t < d / 2 )
                return BounceEaseOut( t * 2, b, c / 2, d );
            return BounceEaseIn( ( t * 2 ) - d, b + c / 2, c / 2, d );
        }

        /// <summary>
        /// Easing equation function for a bounce (exponentially decaying parabolic bounce) easing out/in: 
        /// deceleration until halfway, then acceleration.
        /// </summary>
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double BounceEaseOutIn( double t ) {
            if ( t < .5 )
                return BounceEaseOut( t * 2 ) * .5;
            return .5 + BounceEaseIn( ( t * 2 ) - 1 ) * .5;
        }
        #endregion

        #region Back

        /// <summary>
        /// Easing equation function for a back (overshooting cubic easing: (s+1)*t^3 - s*t^2) easing out: 
        /// decelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double BackEaseOut( double t, double b, double c, double d ) {
            return c * ( ( t = t / d - 1 ) * t * ( ( 1.70158 + 1 ) * t + 1.70158 ) + 1 ) + b;
        }

        /// <summary>
        /// Easing equation function for a back (overshooting cubic easing: (s+1)*t^3 - s*t^2) easing out: 
        /// decelerating from zero velocity.
        /// </summary>
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double BackEaseOut( double t ) {
            return ( t = t - 1 ) * t * ( ( 1.70158 + 1 ) * t + 1.70158 ) + 1;
        }

        /// <summary>
        /// Easing equation function for a back (overshooting cubic easing: (s+1)*t^3 - s*t^2) easing in: 
        /// accelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double BackEaseIn( double t, double b, double c, double d ) {
            return c * ( t /= d ) * t * ( ( 1.70158 + 1 ) * t - 1.70158 ) + b;
        }

        /// <summary>
        /// Easing equation function for a back (overshooting cubic easing: (s+1)*t^3 - s*t^2) easing in: 
        /// accelerating from zero velocity.
        /// </summary>
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double BackEaseIn( double t ) {
            return t * t * ( ( 1.70158 + 1 ) * t - 1.70158 );
        }

        /// <summary>
        /// Easing equation function for a back (overshooting cubic easing: (s+1)*t^3 - s*t^2) easing in/out: 
        /// acceleration until halfway, then deceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double BackEaseInOut( double t, double b, double c, double d ) {
            double s = 1.70158;
            if ( ( t /= d / 2 ) < 1 )
                return c / 2 * ( t * t * ( ( ( s *= ( 1.525 ) ) + 1 ) * t - s ) ) + b;
            return c / 2 * ( ( t -= 2 ) * t * ( ( ( s *= ( 1.525 ) ) + 1 ) * t + s ) + 2 ) + b;
        }

        /// <summary>
        /// Easing equation function for a back (overshooting cubic easing: (s+1)*t^3 - s*t^2) easing in/out: 
        /// acceleration until halfway, then deceleration.
        /// </summary>
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double BackEaseInOut( double t ) {
            double s = 1.70158;
            if ( ( t *= 2 ) < 1 )
                return .5 * t * t * ( ( ( s *= ( 1.525 ) ) + 1 ) * t - s );
            return .5 * ( ( t -= 2 ) * t * ( ( ( s *= ( 1.525 ) ) + 1 ) * t + s ) + 2 );
        }

        /// <summary>
        /// Easing equation function for a back (overshooting cubic easing: (s+1)*t^3 - s*t^2) easing out/in: 
        /// deceleration until halfway, then acceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static double BackEaseOutIn( double t, double b, double c, double d ) {
            if ( t < d / 2 )
                return BackEaseOut( t * 2, b, c / 2, d );
            return BackEaseIn( ( t * 2 ) - d, b + c / 2, c / 2, d );
        }

        /// <summary>
        /// Easing equation function for a back (overshooting cubic easing: (s+1)*t^3 - s*t^2) easing out/in: 
        /// deceleration until halfway, then acceleration.
        /// </summary>
        /// <param name="t">Lerp value</param>
        /// <returns>The correct value.</returns>
        public static double BackEaseOutIn( double t ) {
            if ( t < .5 )
                return BackEaseOut( t * 2 ) * .5;
            return .5 + BackEaseIn( ( t * 2 ) - 1 ) * .5;
        }
        #endregion

        #endregion
    }
}