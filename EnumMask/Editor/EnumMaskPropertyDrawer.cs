﻿/* EnumMaskPropertyDrawer.cs
 * Copyright Eddie Cameron & Grasshopper 2013
 * ----------------------------
 *
 */

using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomPropertyDrawer(typeof(EnumMaskAttribute))]
public class EnumMaskPropertyDrawer : PropertyDrawer
{
    public override void OnGUI( Rect position, SerializedProperty prop, GUIContent label )
    {
        prop.intValue = EditorGUI.MaskField( position, label, prop.intValue, prop.enumNames );
    }

}