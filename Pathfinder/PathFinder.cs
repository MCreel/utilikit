﻿/* PathFinder.cs
 * Copyright Eddie Cameron & Grasshopper 2013
 * ----------------------------
 *
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class Pather<TPatherNode>
{
    public bool printDebug;

    public struct PatherPathInfo
    {
        public TPatherNode[] nodes;
        public float[] costs;
        public float totalCost;

        public static PatherPathInfo Empty
        {
            get
            {
                PatherPathInfo empty;
                empty.nodes = new TPatherNode[0];
                empty.costs = new float[0];
                empty.totalCost = 0;
                return empty;
            }
        }
    }

    public PatherPathInfo FindPath( TPatherNode fromNode, TPatherNode toNode ) {
        return FindPath( fromNode, new[] { toNode } );
    }

    public PatherPathInfo FindPath( TPatherNode fromNode, IList<TPatherNode> possibleNodes )
    {
        var evaluated = new List<TPatherNode>();
        var toEvaluate = new PriorityQueue<float, TPatherNode>();
        var cameFrom = new Dictionary<TPatherNode, NodeToNodeInfo>();

        var costTo = new Dictionary<TPatherNode, float>();
        var costFrom = new Dictionary<TPatherNode, float>();

        var totalCost = new Dictionary<TPatherNode, float>();

        // seed A*
        costTo.Add( fromNode, 0 );
        TPatherNode closestNode;
        float distToClosest = GetHeuristicClosestNodeAndDist( fromNode, possibleNodes, out closestNode );
        costFrom.Add( fromNode, distToClosest );
        totalCost.Add( fromNode, distToClosest );
        toEvaluate.Enqueue( distToClosest, fromNode );

        if ( printDebug )
            DebugExtras.Log( "Evaluating path from " + fromNode + " to " + possibleNodes[0] + ( possibleNodes.Count > 1 ? ", etc" : "" ) );

        // A* loop
        while ( !toEvaluate.IsEmpty )
        {
            TPatherNode evaluating = toEvaluate.DequeueValue(); // get node with lowest total cost

            if ( printDebug )
                DebugExtras.Log( "Evaluating node " + evaluating );

            // test if reachedDest
            foreach ( var node in possibleNodes )
            {
                if ( IsWithinNode( evaluating, node ) )
                {
                    var pathNodes = GetPathFrom( evaluating, cameFrom );
                    if ( pathNodes.Count == 0 )
                        pathNodes.Add( new NodeToNodeInfo( fromNode, node, 0 ) ); // if dest is same as start, make empty path

                    var path = MakePathInfo( pathNodes );
                    if ( printDebug )
                    {
                        DebugExtras.Log( "Found path costing " + path.totalCost + " from " + fromNode + " to " + node );
                        var sb = new System.Text.StringBuilder();
                        for ( int i = 0; i < path.nodes.Length; i++ )
                            sb.Append( i + ": " + path.nodes[i] );
                        DebugExtras.Log( sb );
                    }
                    return path;
                }
            }

            evaluated.Add( evaluating );

            // get possible next nodes
            var neighbours = GetPossibleNextNodes( evaluating, possibleNodes );
            if ( printDebug )
                DebugExtras.Log( "Found " + neighbours.Count + " possible next nodes" );
            foreach ( var neighbour in neighbours )
            {
                if ( evaluated.Contains( neighbour ) )
                    continue;

                if ( printDebug )
                    DebugExtras.Log( "Testing possible next step to " + neighbour );

                NodeToNodeInfo toNodeInfo;
                if ( !IsNodeValid( evaluating, neighbour, out toNodeInfo ) )
                    continue;

                if ( printDebug )
                    DebugExtras.Log( "Can step from " + evaluating + " to " + neighbour + " at cost " + toNodeInfo.distance );

                try
                {
                    float tentCostTo = costTo[evaluating] + toNodeInfo.distance;
                    if ( !toEvaluate.Contains( neighbour ) )
                    {
                        costTo.Add( neighbour, tentCostTo );
                        TPatherNode closestEnd;
                        float toClosestEnd = GetHeuristicClosestNodeAndDist( neighbour, possibleNodes, out closestEnd );
                        costFrom.Add( neighbour, toClosestEnd );
                        // add neighbour to evaluate
                        if ( printDebug )
                            DebugExtras.Log( "New possible path thru node with cost " + ( tentCostTo + toClosestEnd ) );
                        totalCost.Add( neighbour, tentCostTo + toClosestEnd );
                        toEvaluate.Enqueue( tentCostTo + toClosestEnd, neighbour );
                        cameFrom.Add( neighbour, toNodeInfo );
                    }
                    else if ( tentCostTo < costTo[neighbour] )
                    {
                        costTo[neighbour] = tentCostTo;
                        float newTotalCost = tentCostTo + costFrom[neighbour];
                        if ( printDebug )
                            DebugExtras.Log( "Found cheaper path thru node costing " + newTotalCost + " down from " + totalCost[neighbour] );
                        totalCost[neighbour] = newTotalCost;
                        toEvaluate.TryChangePriority( neighbour, newTotalCost );
                        cameFrom[neighbour] = toNodeInfo;
                    }
                    else if ( printDebug )
                        DebugExtras.Log( "New path thru node not cheaper than existing" );
                }
                catch ( System.Exception e )
                {
                    DebugExtras.LogWarning( "Error evaluating node " + evaluating + " to neighbour " + neighbour );
                    DebugExtras.LogError( e );
                }
            }
        }

        // finished evaluating all reachable nodes without reacing target
        // NO PATH
        return PatherPathInfo.Empty;
    }

    List<NodeToNodeInfo> GetPathFrom( TPatherNode endNode, Dictionary<TPatherNode, NodeToNodeInfo> cameFromMap )
    {
        List<NodeToNodeInfo> pathNodes;
        NodeToNodeInfo lastNode;
        if ( cameFromMap.TryGetValue( endNode, out lastNode ) )
        {
            pathNodes = GetPathFrom( lastNode.fromNode, cameFromMap );
            pathNodes.Add( lastNode );
        }
        else
            pathNodes = new List<NodeToNodeInfo>();
        
        return pathNodes;
    }

    PatherPathInfo MakePathInfo( IList<NodeToNodeInfo> fromNodeDistances )
    {
        PatherPathInfo pathInfo = new PatherPathInfo();
        if ( fromNodeDistances.Count > 0 )
        {
            pathInfo.nodes = new TPatherNode[fromNodeDistances.Count + 1];
            pathInfo.costs = new float[fromNodeDistances.Count + 1];
            pathInfo.totalCost = 0;

            pathInfo.nodes[0] = fromNodeDistances[0].fromNode;
            pathInfo.costs[0] = 0;
            for ( int i = 0; i < fromNodeDistances.Count; i++ )
            {
                pathInfo.nodes[i + 1] = fromNodeDistances[i].toNode;
                pathInfo.costs[i + 1] = fromNodeDistances[i].distance;
                pathInfo.totalCost += fromNodeDistances[i].distance;
            }
        }
        else
        {
            DebugExtras.LogWarning( "Making path info out of zero length path" );
            pathInfo.nodes = new TPatherNode[0];
            pathInfo.costs = new float[0];
        }
        return pathInfo;
    }

    protected float GetHeuristicClosestNodeAndDist( TPatherNode fromNode, IList<TPatherNode> possibleNodes, out TPatherNode closestNode )
    {
        closestNode = default( TPatherNode );
        float closestDist = float.MaxValue;
        foreach ( var node in possibleNodes )
        {
            var dist = HeuristicDist( fromNode, node );
            if ( dist < closestDist )
            {
                closestNode = node;
                closestDist = dist;
            }
        }
        return closestDist;
    }

    protected abstract float HeuristicDist( TPatherNode fromNode, TPatherNode toNode );

    protected abstract bool IsWithinNode( TPatherNode nodeToTest, TPatherNode isWithin );

    /// <summary>
    /// Get all nodes that are reachable from <paramref name="fromNode"/>
    /// </summary>
    /// <remarks>
    /// Do any expensive evaluation in <see cref="IsNodeValid"/>, since some nodes will be culled before then
    /// </remarks>
    /// <param name="fromNode">Node to find neighbours of</param>
    /// <param name="targetNodes">Possible endpoints of entire path (if needed)</param>
    /// <returns>List of possible next nodes</returns>
    protected abstract ICollection<TPatherNode> GetPossibleNextNodes( TPatherNode fromNode, IEnumerable<TPatherNode> targetNodes );

    protected struct NodeToNodeInfo
    {
        public TPatherNode fromNode, toNode;
        public float distance;

        public NodeToNodeInfo( TPatherNode fromNode, TPatherNode toNode, float distance )
        {
            this.fromNode = fromNode;
            this.toNode = toNode;
            this.distance = distance;
        }
    }
    /// <summary>
    /// Evaluate any necessary information (eg distance) about travel between <paramref name="fromNode"/> and <paramref name="toNode"/>
    /// </summary>
    /// <param name="fromNode">Start node</param>
    /// <param name="toNode">Dest node</param>
    /// <param name="nodeInfo">Information about step between nodes</param>
    /// <returns>Whether dest node is valid next step from start node</returns>
    protected abstract bool IsNodeValid( TPatherNode fromNode, TPatherNode toNode, out NodeToNodeInfo nodeInfo );
}