/* ActionTime.cs
 * Copyright Eddie Cameron & Grasshopper 2014
 * Open-sourced under the MIT licence
 * ----------------------------
 * 
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Pauseable time implimentation, use instead of UnityEngine.Time
/// </summary>
public class ActionTime : StaticBehaviour<ActionTime>
{
    /// <summary>
    /// Is ActionTime currently progressing
    /// </summary>
    public static bool isPaused { get; private set; }
    /// <summary>
    /// ActionTime elapsed since last frame
    /// </summary>
    public static float deltaTime { get; private set; }
    /// <summary>
    /// ActionTime elapsed since game start
    /// </summary>
    public static float time { get; private set; }

    /// <summary>
    /// How fast ActionTime progresses wrt Time.time
    /// </summary>
    public static float timeScale
    {
        get
        {
            return _timeScale;
        }
        set
        {
            if ( _timeScale != value )
            {
                if ( value == 0 )
                    Pause();
                else if ( isPaused )
                    UnPause( value );
                else
                    _timeScale = value;
            }
        }
    }
    static float _timeScale = 1f;

    /// <summary>
    /// When ActionTime is paused
    /// </summary>
    public static event System.Action Paused;
    /// <summary>
    /// When ActionTime is unpaused
    /// </summary>
    public static event System.Action UnPaused;

    protected override void LateUpdate()
    {
        base.LateUpdate();

        deltaTime = Time.deltaTime * timeScale;
        time += deltaTime;
    }

    /// <summary>
    /// Pause ActionTime, if unpaused
    /// </summary>
    public static void Pause()
    {
        if ( !isPaused )
        {
            isPaused = true;
            _timeScale = 0;
            if ( Paused != null )
                Paused();
        }
    }

    /// <summary>
    /// Unpause ActionTime, if paused
    /// </summary>
    /// <param name="toTimeScale">Defaults to 1 (realtime)</param>
    public static void UnPause( float toTimeScale = 1f )
    {
        if ( isPaused )
        {
            isPaused = false;
            _timeScale = toTimeScale;
            if ( UnPaused != null )
                UnPaused();
        }
    }

    /// <summary>
    /// Wait one frame if unpaused, else wait until unpaused
    /// Equiv to <c>yield return 0;</c>
    /// </summary>
    /// <returns></returns>
    public static Coroutine WaitOneActionFrame()
    {
        return instance.StartCoroutine( _WaitOneActionFrame() );
    }

    static IEnumerator _WaitOneActionFrame()
    {
        do
        {
            yield return 0;
        } while ( isPaused );
    }

    /// <summary>
    /// Wait for <paramref name="secondsToWait"/> seconds, scaled by ActionTime.timescale
    /// Yields indefinitely if paused
    /// </summary>
    /// <param name="secondsToWait"></param>
    /// <returns></returns>
    public static Coroutine WaitForActionSeconds( float secondsToWait )
    {
        return instance.StartCoroutine( _WaitForActionSeconds( secondsToWait ) );
    }

    static IEnumerator _WaitForActionSeconds( float secondsToWait )
    {
        while ( secondsToWait > 0 )
        {
            if ( !isPaused )
                secondsToWait -= Time.deltaTime;

            yield return 0;
        }
    }
}